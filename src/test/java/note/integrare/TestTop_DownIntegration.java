package note.integrare;

import note.controller.NoteController;
import note.model.Corigent;
import note.model.Elev;
import note.model.Medie;
import note.model.Nota;
import note.utils.ClasaException;
import note.utils.Constants;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static note.utils.Constants.invalidMateria;
import static org.junit.Assert.assertEquals;

public class TestTop_DownIntegration {
    private NoteController ctrl;

    @Before
    public void setUp() throws Exception {
        ctrl = new NoteController();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testAInt() throws ClasaException {
        //Integrarea modului A
        //testare unitara pentru modulul A (A-valid)
        Nota nota1 = new Nota(2, "Analiza Matematica", 1);
        Nota nota2 = new Nota(2,"Franceza", 3);
        Nota nota3 = new Nota(2,"Franceza", 4);
        ctrl.addNota(nota1);
        ctrl.addNota(nota2);
        ctrl.addNota(nota3);
        assertEquals(3, ctrl.getNote().size());
        assertEquals(ctrl.getNote().get(2).getNrmatricol(), 2,0.001);
        assertEquals(ctrl.getNote().get(2).getMaterie(), "Franceza");
        assertEquals(ctrl.getNote().get(2).getNota(), 4, 0.001);
    }

    @Test
    public void testA() throws ClasaException {
        //testare unitara A (A-invalid)
        //lungime materie inafara intervalului 5..20 de caractere (limita inferioara)
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(invalidMateria);
        Nota nota = new Nota(68, "Info", 10);
        ctrl.addNota(nota);
    }

    @Test
    public void testB()throws  ClasaException{
        //testare unitara B (B-valid)
        Elev e1 = new Elev(1, "Elev1");
        Elev e2 = new Elev(2, "Elev2");
        ctrl.addElev(e1);
        ctrl.addElev(e2);
        Nota nota1 = new Nota(1,"Romana",10);
        Nota nota2 = new Nota(2,"Engleza",8);
        ctrl.addNota(nota1);
        ctrl.addNota(nota2);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Medie> rezultate = ctrl.calculeazaMedii();
        assertEquals(rezultate.size(),2);
    }

    @Test
    public void testC()throws ClasaException{
        //testare unitara C (C-valid)
        Elev e1 = new Elev(1, "Elev1");
        Elev e2 = new Elev(2, "Elev2");
        ctrl.addElev(e1);
        ctrl.addElev(e2);
        Nota nota1 = new Nota(1,"Romana",4);
        Nota n1 = new Nota(1,"Romana",4);
        Nota nota2 = new Nota(2,"Engleza",5);
        Nota n2 = new Nota(2,"Engleza", 3);
        ctrl.addNota(nota1);
        ctrl.addNota(n1);
        ctrl.addNota(nota2);
        ctrl.addNota(n2);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Corigent> corigenti = ctrl.getCorigenti();
        assertEquals(corigenti.get(1).getNrMaterii(), corigenti.get(0).getNrMaterii());
    }

    @Test
    public void testBAInt() throws ClasaException{
        //testare de integrare a modulului B, A
        //P->B->A A-valid B-invalid
        //B
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.emptyRepository);
        ctrl.calculeazaMedii();
        //A
        Nota nota1 = new Nota(1000, "Desena", 10);
        ctrl.addNota(nota1);
        assertEquals(ctrl.getNote().size(),1);
    }

    @Test
    public void testCombinate() throws ClasaException{
        //testare de integrare a modulului C, B, A
        //P->B->A->C B-invalid A-valid C-valid
        //B
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.emptyRepository);
        List<Medie> rezultate = ctrl.calculeazaMedii();
        //A
        Elev e1 = new Elev(1, "Elev1");
        ctrl.addElev(e1);
        Nota nota = new Nota(1, "Desena", 4);
        ctrl.addNota(nota);
        assertEquals(1, ctrl.getNote().size());
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        //C
        List<Corigent> corigenti1 = ctrl.getCorigenti();
        assertEquals(corigenti1.size(),1);
    }

    @Test
    public void testMethod() throws ClasaException{
        testA();
        testB();
        testC();
        testAInt();
        testBAInt();
        testCombinate();
    }
}
