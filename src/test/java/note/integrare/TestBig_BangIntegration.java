package note.integrare;

import note.controller.NoteController;
import note.model.Corigent;
import note.model.Elev;
import note.model.Medie;
import note.model.Nota;
import note.utils.ClasaException;
import note.utils.Constants;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static note.utils.Constants.invalidMateria;
import static org.junit.Assert.assertEquals;

public class TestBig_BangIntegration {
    private NoteController ctrl;

    @Before
    public void setUp() throws Exception {
        ctrl = new NoteController();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testA() throws ClasaException {
        //testare unitara A (A-invalid)
        //lungime materie inafara intervalului 5..20 de caractere (limita inferioara)
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(invalidMateria);
        Nota nota = new Nota(68, "Info", 10);
        ctrl.addNota(nota);
    }

    @Test
    public void testB()throws  ClasaException{
        //testare unitara B (B-valid)
        Elev e1 = new Elev(1, "Elev1");
        Elev e2 = new Elev(2, "Elev2");
        ctrl.addElev(e1);
        ctrl.addElev(e2);
        Nota nota1 = new Nota(1,"Romana",10);
        Nota nota2 = new Nota(2,"Engleza",8);
        ctrl.addNota(nota1);
        ctrl.addNota(nota2);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Medie> rezultate = ctrl.calculeazaMedii();
        assertEquals(rezultate.size(),2);
    }

    @Test
    public void testC()throws ClasaException{
        //testare unitara C (C-valid)
        Elev e1 = new Elev(1, "Elev1");
        Elev e2 = new Elev(2, "Elev2");
        ctrl.addElev(e1);
        ctrl.addElev(e2);
        Nota nota1 = new Nota(1,"Romana",4);
        Nota n1 = new Nota(1,"Romana",4);
        Nota nota2 = new Nota(2,"Engleza",5);
        Nota n2 = new Nota(2,"Engleza", 3);
        ctrl.addNota(nota1);
        ctrl.addNota(n1);
        ctrl.addNota(nota2);
        ctrl.addNota(n2);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Corigent> corigenti = ctrl.getCorigenti();
        assertEquals(corigenti.get(1).getNrMaterii(), corigenti.get(0).getNrMaterii());
    }

    @Test
    public void testCombinare() throws ClasaException{
        testA();
        testB();
        testC();
        //Aici se face integrarea cu B, C, A
        //|P -> B -> C -> A| B-valid; C-valid; A-invalid;
        //B-calcul medii semestriale
        //A-addNota
        //C-elevi corignit
        Elev e1 = new Elev(1, "Elev1");
        ctrl.addElev(e1);
        Nota nota = new Nota(1, "Romana", 10);
        ctrl.addNota(nota);
        assertEquals(1, ctrl.getNote().size());
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        //B
        List<Medie> rezultate = ctrl.calculeazaMedii();
        assertEquals(1, rezultate.size());
        //C
        List<Corigent> corigenti = ctrl.getCorigenti();
        assertEquals(corigenti.size(),0);
        //A
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidMateria);
        Nota nota1 = new Nota(1, "Info", 5);
        ctrl.addNota(nota1);
    }

    /*@Test
    public void testMethod() throws ClasaException{
        testA();
        testB();
        testC();
        testCombinare();
    }*/
}
