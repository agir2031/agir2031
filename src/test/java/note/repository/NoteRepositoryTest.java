package note.repository;

import note.controller.NoteController;
import note.model.Nota;
import note.utils.ClasaException;
import note.utils.Constants;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static note.utils.Constants.invalidNrmatricol;
import static org.junit.Assert.assertEquals;

public class NoteRepositoryTest {
    private NoteController ctrl;

    @Before
    public void init() {
        ctrl = new NoteController();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();


    @Test
    public void test1() throws Exception {
        Nota nota = new Nota(1, "Analiza1", 1);
        Nota n1 = new Nota(1, "Materie1", 1);
        Nota n2 = new Nota(1, "Materie1", 7);
        try {
            ctrl.addNota(nota);
            ctrl.addNota(n1);
            ctrl.addNota(n2);
        } catch (ClasaException e) {
            throw e;
        }
        assertEquals(3, ctrl.getNote().size());
        //to-DO : DE vf daca ob adaugat = obiectul care trabuia sa se adauge
    }

    @Test
    public void test2() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(invalidNrmatricol);
        Nota nota = new Nota(10.1, "Istorie", 5);
        ctrl.addNota(nota);
    }

    @Test
    public void test3() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNrmatricol);
        Nota nota = new Nota(0, "Matem", 5);
        ctrl.addNota(nota);
    }

    @Test
    public void test4() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidMateria);
        Nota nota = new Nota(1, "Mate", 5);
        ctrl.addNota(nota);
    }


    @Test
    public void test5() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNota);
        Nota nota = new Nota(2, "Geografie", -2);
        ctrl.addNota(nota);
    }

    @Test
    public void test6() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNota);
        Nota nota = new Nota(2, "Geografie", 7.3);
        ctrl.addNota(nota);
    }


    @Test
    public void test7() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(invalidNrmatricol);
        Nota nota = new Nota(0, "Istorie", 5);
        ctrl.addNota(nota);
    }

    @Test
    public void test8() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNota);
        Nota nota = new Nota(1, "Romana", 0);
        ctrl.addNota(nota);
    }

    @Test
    public void test9() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNrmatricol);
        Nota nota = new Nota(1001, "abcdef", 5);
        ctrl.addNota(nota);
    }

    @Test
    public void test10() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidMateria);
        Nota nota = new Nota(1, "111111111111111111111", 9);
        ctrl.addNota(nota);
    }//21*1

    @Test
    public void test11() throws Exception {
        Nota nota = new Nota(2, "algebra", 1);
        try {
            ctrl.addNota(nota);
        } catch (ClasaException e) {
            throw e;
        }
        assertEquals(1, ctrl.getNote().size());

    }


}
